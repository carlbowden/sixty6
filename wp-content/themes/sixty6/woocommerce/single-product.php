<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
} ?>

<div class="col-sm-7 bg-page-grey">
    <main class="content main">
        <h3>Purchase Tickets</h3>
            <?php
                /**
                 * woocommerce_before_main_content hook.
                 *
                 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
                 * @hooked woocommerce_breadcrumb - 20
                 */
                do_action( 'woocommerce_before_main_content' );
            ?>
        
                <?php while ( have_posts() ) : the_post(); ?>
        
                    <?php wc_get_template_part( 'content', 'single-product' ); ?>
        
                <?php endwhile; // end of the loop. ?>
        
            <?php
                /**
                 * woocommerce_after_main_content hook.
                 *
                 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
                 */
                do_action( 'woocommerce_after_main_content' );
            ?>
        
        <script>
            
            (function ($) {
        
                //$('.btn-bulk').click(function () {
                    $('div.product').slideUp('200', function () {
                        $('.variations_form').hide();
                        $('#matrix_form').slideDown('400', function () {
                            $('#qty_input_0').focus();
                        });
                    });
               // });
        
            })(jQuery);
            
        </script>

    </main><!-- /.main -->


</div>
<div class="col-sm-5 right-sidebar">
    <div class="top-gallery">
        
        <?php
        // loop through the banners
        $gallery = get_field( 'top_gallery' );
        if ( is_array( $gallery ) ):
            foreach ( $gallery as $image ): ?>
                <div class="gallery-slide" style="  background-image: url(<?= $image['url']; ?>);"></div>
            <?php endforeach; ?>
        <?php else: ?>
            <div class="gallery-slide" style="background-image: url(/wp-content/uploads/2017/01/front-sixty6.jpg);"></div>
            <div class="gallery-slide" style="background-image: url(/wp-content/uploads/2017/01/img_1149.jpg);"></div>
            <div class="gallery-slide" style="background-image: url(/wp-content/uploads/2017/01/mashina-main.jpg);"></div>
            <div class="gallery-slide" style="background-image: url(/wp-content/uploads/2017/01/mashina140416_056.jpg);"></div>
            <div class="gallery-slide" style="background-image: url(/wp-content/uploads/2017/01/mashina140416_066.jpg);"></div>
        <?php endif; ?>

    </div>
    <div class="embed-container">
        <?php $video = get_field( 'video' ); ?>
        <?php if ( isset( $video ) && $video ):
            echo $video;
        else:?>
            <div id="fb-root"></div>
            <script>(function (d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) return;
                    js = d.createElement(s);
                    js.id = id;
                    js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.3";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>
            <div class="fb-video" data-href="https://www.facebook.com/Sixty6onPeterborough/videos/678533892312748/" data-width="640">
                <blockquote cite="https://www.facebook.com/Sixty6onPeterborough/videos/678533892312748/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Sixty6onPeterborough/videos/678533892312748/">Where Events Come to Life!</a>
                    <p></p>Posted by <a href="https://www.facebook.com/Sixty6onPeterborough/">Sixty6 on Peterborough</a> on Tuesday, 20 September 2016
                </blockquote>
            </div>
        <?php endif; ?>
    </div>
</div>
