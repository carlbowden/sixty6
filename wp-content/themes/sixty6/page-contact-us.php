<?php
/**
 * Template Name: Contact us Page template
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', 'page-contact-us'); ?>
<?php endwhile; ?>
