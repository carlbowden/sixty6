
<footer class="black-bar-footer content-info">
  <div class="containerr-fluid">
      <div class="row">
          <div class="col-sm-9">
	          <?php
	          if ( has_nav_menu( 'footer_navigation' ) ) :
		          wp_nav_menu( [ 'theme_location' => 'footer_navigation', 'menu_class' => 'nav footer-nav' ] );
	          endif;
	          ?>
          </div>
          <div class="col-sm-3 textright hidden-xs">
              <ul class="social">
                  <li><a href="https://www.facebook.com/Sixty6onPeterborough"><img src="/wp-content/themes/sixty6/dist/images/facebook-icon_svg.svg" alt=""></a></li>
                  <li><a href=""><img src="/wp-content/themes/sixty6/dist/images/youtube-icon_svg.svg" alt=""></a></li>
              </ul>
          </div>
          <div class="col-xs-12 textleft visible-xs">
              <ul class="social">
                  <li><a href="https://www.facebook.com/Sixty6onPeterborough"><img src="/wp-content/themes/sixty6/dist/images/facebook-icon_svg.svg" alt=""></a></li>
                  <li><a href=""><img src="/wp-content/themes/sixty6/dist/images/youtube-icon_svg.svg" alt=""></a></li>
              </ul>
          </div>
      </div>
      <div class="row">
          <div class="col-sm-12 copyright">
			  &copy;<?=date('Y'); ?>, SIXTY6. ALL RIGHTS RESERVED.
          </div>
      </div>
    
  </div>
</footer>
