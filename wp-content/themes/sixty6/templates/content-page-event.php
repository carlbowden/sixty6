<div class="col-sm-7 bg-page-grey">
	<main class="content main">
        < <a href="/events">BACK TO EVENTS</a>
		<?php get_template_part( 'templates/page', 'header' ); ?>
        
        <div class="evnts-details">
            <p class="top">
                <span class="top-left">DATE:</span> <?php the_field( 'start_date' ); ?>&mdash;<?php the_field( 'end_date' ); ?>
                <span class="top-right">TIME:</span> <?php the_field( 'start_time' ); ?>
            </p>
            
            <span>WHERE:</span> 66 Peterborough Street , Christchurch<br>
            <span>Restrictions:</span> no restrictions<br>
        </div>
        
        
		<?php the_content(); ?>
  
        <?php if(get_field('whats_on_buy_tickets_link')): ?>
        
            <div class="buy-tickets-box">
                <a href="<?php the_field( 'whats_on_buy_tickets_link' ); ?>"><span class="icon-ticket-icon"></span>  PURCHASE TICKETS</a>
            </div>
        
        <?php endif; ?>
  
	</main><!-- /.main -->
    
    <!-- todo related events -->
    <?php if(get_field('show_under_content_panel')): ?>
    
    <div class="under_content">
        <main class="content">
            <h4><?= get_field( 'under_heading' ); ?></h4>
            <?= get_field( 'under_content' ); ?>
        </main>
    </div>
    
    <?php endif; ?>
    
</div>
<div class="col-sm-5 right-sidebar">
    <div class="top-gallery">
        
        <?php
        // loop through the banners
        $gallery = get_field('top_gallery');
        foreach ( $gallery as $image ): ?>
            <div class="gallery-slide" style="background-image: url(<?= $image['url']; ?>);">
            
            </div>
        <?php endforeach; ?>
        
    </div>
    <div class="embed-container">
		<?php the_field( 'video' ); ?>
    </div>
</div>
