<div class="col-sm-7">
    <div class="up-comming-heading">
        <h2>UPCOMING EVENTS</h2>
    </div>
    <div class="up-comming-gallery">
		
		<?php
	
	    // check if the repeater field has rows of data
	    if ( have_rows( 'up_coming_events_slider' ) ):
		
		    // loop through the rows of data
		    while ( have_rows( 'up_coming_events_slider' ) ) : the_row();
			
			    // display a sub field value
			    
			    
			    ?>
                <div class="gallery-slide" style="background-image: url(<?= get_sub_field( 'image' ); ?>);">
                    <a href="<?= get_sub_field( 'link' ); ?>">
                        <div class="title">
                           <h2><?= get_sub_field('upcoming_heading'); ?></h2>
                            <h5><?= get_sub_field( 'sub_heading' ); ?></h5>
                        </div>
                    </a>
                </div>
        
        <?php  endwhile;
	
	    else :
		
		    // no rows found
	
	    endif;
	
	    ?>
    </div>
    
</div>
<div class="col-sm-5 right-sidebar">
   
    <div class="our-food" style="background-image: url(<?= get_field( 'our_food_image' ); ?>);">
        <a href="<?= get_field('link'); ?>">
            <div class="title">
                <h2><?= get_field('heading'); ?></h2>
            </div>
        </a>
    </div>
    
    <div class="right_gallery">
        
        <?php
        // loop through the banners
        $gallery = get_field('right_gallery');
        if(is_array($gallery)) foreach ( $gallery as $image ): ?>
            <div class="gallery-slide" style="background-image: url(<?= $image['url']; ?>);">
                <div class="title">
                    <h2>Gallery</h2>
                </div>
                
            </div>
        <?php endforeach; ?>
       
    </div>
    <?php

    // check if we have video data
    


    $videos = get_field('right_video');
    $vid = $videos[0]['video_id'];
    
    ?>

    <div class="embed-container">
      <iframe style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; border: none; z-index: 100"
              src="https://www.youtube.com/embed/<?= $vid?>?controls=0&showinfo=0&rel=0&autoplay=0&loop=1" width="420" height="315"
              allowfullscreen="allowfullscreen"></iframe>
    </div>


    
</div>
