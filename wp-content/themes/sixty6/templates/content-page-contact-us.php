<div class="col-sm-7 bg-page-grey">
	<main class="content main with-under-content">

		<?php get_template_part( 'templates/page', 'header' ); ?>
		<?php the_content(); ?>
        
        
        <!-- accordans -->
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-inverse">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                        <a class="collapsed"  role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseGeneral" aria-expanded="false" aria-controls="collapseGeneral">
                            I HAVE A GENERAL ENQUIRY
                            <span class="open">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                    <path d="M0 7.33l2.829-2.83 9.175 9.339 9.167-9.339 2.829 2.83-11.996 12.17z"/>
                                </svg>
                            </span>
                        </a>
                    </h4>
                </div>
                <div id="collapseGeneral" class="panel-collapse collapse" role="tabpanel" aria-labelledby="collapseGeneral">
                    <div class="panel-body">
            
    
                        <?= gravity_form( 1, $display_title = FALSE, $display_description = TRUE, $display_inactive = FALSE, $field_values = NULL, $ajax = TRUE, $tabindex, $echo = FALSE ); ?>

                    </div>
                </div>
            </div>
            <div class="panel panel-inverse">
                <div class="panel-heading" role="tab" id="headingTwo">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseBooking" aria-expanded="false" aria-controls="collapseBooking">
                            I WANT TO ENQUIRE ABOUT BOOKING AN EVENT
                            <span class="open">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                    <path d="M0 7.33l2.829-2.83 9.175 9.339 9.167-9.339 2.829 2.83-11.996 12.17z"/>
                                </svg>
                            </span>
                        </a>
                    </h4>
                </div>
                <div id="collapseBooking" class="panel-collapse collapse" role="tabpanel" aria-labelledby="collapseBooking">
                    <div class="panel-body">
                        <?= gravity_form( 2, $display_title = FALSE, $display_description = TRUE, $display_inactive = FALSE, $field_values = NULL, $ajax = TRUE, $tabindex, $echo = FALSE ); ?>

                    </div>
                </div>
            </div>
            
        </div>
        
        
        
        
       
        
	</main><!-- /.main -->
    
    <?php if(get_field('show_under_content_panel')): ?>
    
    <div class="under_content contact-us">
        <main class="content with-under-content">
            <h4><?= get_field( 'under_heading' ); ?></h4>
            <p><a href="tel:<?= get_field( 'phone' ); ?>">
                    <span class="contact-address-icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M20 22.621l-3.521-6.795c-.008.004-1.974.97-2.064 1.011-2.24 1.086-6.799-7.82-4.609-8.994l2.083-1.026-3.493-6.817-2.106 1.039c-7.202 3.755 4.233 25.982 11.6 22.615.121-.055 2.102-1.029 2.11-1.033z"/>
                    </svg> <?= get_field( 'phone' ); ?></span></a></p>
            <p><a href="mailto:<?= get_field( 'email' ); ?>">
                    <span class="contact-address-icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M0 3v18h24v-18h-24zm6.623 7.929l-4.623 5.712v-9.458l4.623 3.746zm-4.141-5.929h19.035l-9.517 7.713-9.518-7.713zm5.694 7.188l3.824 3.099 3.83-3.104 5.612 6.817h-18.779l5.513-6.812zm9.208-1.264l4.616-3.741v9.348l-4.616-5.607z"/>
                    </svg></span>
                    <?= get_field( 'email' ); ?></a></p>
            
        </main>
    </div>
    
    <?php endif; ?>
    
</div>
<div class="col-sm-5 right-sidebar contact-us">
    <div class="address-icon">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
            <path d="M12 0c-4.198 0-8 3.403-8 7.602 0 4.198 3.469 9.21 8 16.398 4.531-7.188 8-12.2 8-16.398 0-4.199-3.801-7.602-8-7.602zm0 11c-1.657 0-3-1.343-3-3s1.343-3 3-3 3 1.343 3 3-1.343 3-3 3z"/>
        </svg>
    </div>
    <div class="address-details">
        <?= get_field('right_side_address_details');?>
    </div>
    <div id="map"></div>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD31AgNX4kYHOBBPAfGiHuibWzFlK_XUkE"></script>
   <!-- <script src="/wp-content/themes/sixty6/dist/scripts/gmaps.min.js"></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gmaps.js/0.4.25/gmaps.js"></script>
   
    <script type="text/javascript">
        (function ($) {
            map = new GMaps({
                div: '#map',
                lat: -43.52548,
                lng: 172.6318806,
                scrollwheel: false
            });

            map.addMarker({
                lat: -43.52548,
                lng: 172.66318806,
                title: 'Sixty6 on Peterborough\n66 Peterborough Street,\nChristchurch, New Zealand',
                click: function (e) {
                    //alert('You clicked in this marker');
                },
                infoWindow: {
                  content: '<p>Sixty6<br> CNR PETERBOROUGH & DURHAM ST,<br>CHRISTCHURCH,<br>NEW ZEALAND</p>'
                }
            });

        })(jQuery);
    </script>
</div>
