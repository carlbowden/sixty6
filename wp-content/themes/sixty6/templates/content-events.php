<div class="whats-on-section bg-inverse ">
        <!-- filter -->
        <?php

        wp_reset_query();
        
        $args = array(
            'post_type' => 'whats_on',
        );
        
        // starting values
        $today = date( 'Ymd' ); // W is week #, w is day of week #
        $start = $today;
        $end   = $today;
        $all   = FALSE;
        
        $active = 'all';
        
        if ( isset( $_GET['today'] ) ) {
            // what day are we on
            
            $start  = $today;
            $end    = $today;
            $active = 'today';
            
        } else if ( isset( $_GET['week'] ) ) {
            // what day are we on
            
            $week = date( 'W' ); // W is week #, w is day of week #
            
            $dto   = new DateTime();
            $start = $dto->setISODate( date( 'Y' ), $week, 0 )->format( 'Ymd' );
            $end   = $dto->setISODate( date( 'Y' ), $week, 6 )->format( 'Ymd' );
            
            $active = 'week';
            
        } else if ( isset( $_GET['month'] ) ) {
            $month    = date( 'm' );
            $last_day = date( 't', mktime( 0, 0, 0, $month, 1, date( 'Y' ) ) );
            // ACF makes these a number of a kind ie 20170120
            $start = date( 'Ymd', mktime( 0, 0, 0, $month, 1, date( 'Y' ) ) );
            $end   = date( 'Ymd', mktime( 0, 0, 0, $month, $last_day, date( 'Y' ) ) );
            
            $active = 'month';
            
          
        } else {
            
            $all = TRUE;
            
            //for order
            $args['meta_query'] = [
                'relation'         => 'AND',
                'start_data_start' => [
                    'key'     => 'start_date',
                    'compare' => 'EXISTS',
                ]
            ];
            
        }
        $args['meta_query'] = [
            'relation'         => 'AND',
            'start_data_start' => [
                'key'     => 'start_date',
                'compare' => 'EXISTS',
            ]
        ];
        
        $args['orderby'] = array(
            'start_data_start' => 'ASC',
        );
        
        
        $loop = new WP_Query( $args ); ?>
        
        <div class="row">
            <div class="col-sm-12 pb-2 whats-on-filters">
                <a href="?today" class="btn btn-lg btn-inverse btn-outline-info<?= ( $active == 'today' ) ? ' active' : ''; ?>">Today</a>
                <a href="?week" class="btn btn-lg btn-inverse btn-outline-info<?= ( $active == 'week' ) ? ' active' : ''; ?>">This week</a>
                <a href="?month" class="btn btn-lg btn-inverse btn-outline-info<?= ( $active == 'month' ) ? ' active' : ''; ?>">This Month</a>
                <a href="?all" class="btn btn-lg btn-inverse btn-outline-info<?= ( $active == 'all' ) ? ' active' : ''; ?>">All</a>
            </div>
        </div>
        
        <?php if ( $loop->have_posts() ) {
            
            while ( $loop->have_posts() ) : $loop->the_post();
                
                $start_date = date( 'Ymd', strtotime( get_field( 'start_date' ) ) );
                $end_date   = date( 'Ymd', strtotime( get_field( 'end_date' ) ) );
                
                if ( ( $all ) || ( $start_date < $end ) && ( $end_date > $start ) ): ?>
                    
                    
                            <div class="event">
                                <div class="row align-middle">
                                    
                                    <div class="col-sm-3">
                                        <?= date( 'D, d M', strtotime( get_field( 'start_date' ) ) ); ?>
                                        <?php if ( get_field( 'end_date' ) ): ?>
                                            to<br> <?= date( 'D, d M', strtotime( get_field( 'end_date' ) ) ); ?>
                                        <?php endif; ?>
                                    </div>
                                    <div class="col-sm-6 pt-1">
                                        <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                        <a href="<?= the_permalink(); ?>">More Info > </a>
                                    </div>
                                    <div class="col-sm-2 text-sm-right pt-1">
                                        
                                        <?php if ( get_field( 'whats_on_buy_tickets_link' ) ): ?>
                                            <a href="<?php the_field( 'whats_on_buy_tickets_link' ); ?>" class="btn btn-lg btn-outline-primary">Buy tickets > </a>
                                        <?php endif; ?>
                                        
                                    </div>
                                </div>
                            </div>
                   
                <?php endif; ?>
            <?php endwhile;
        }
        
        
        ?>
    

</div>
