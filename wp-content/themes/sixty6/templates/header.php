<header class="black-bar hidden-xs">
  <div class="container-fluid">
    <div class="col-sm-4 text-left header-text"><?= get_field( 'left_name', 'option' ); ?></div>
    <div class="col-sm-8 header-text text-right">
        <?= get_field( 'tag_line', 'option' ); ?>
        &nbsp;|&nbsp;
	    <?= get_field( 'address', 'option' ); ?>
        &nbsp;|&nbsp;
        <a href="tel:<?= get_field( 'phone', 'option' ); ?>"><?= get_field( 'phone', 'option' ); ?></a>
    </div>
  </div>
</header>
<header class="mobile  visible-xs">
    <div class="container-fluid">
        <div class="col-xs-12 black-bar text-left header-text">
            <?= get_field('address', 'option'); ?>
            <a href="tel:<?= get_field('phone', 'option'); ?>"><?= get_field('phone', 'option'); ?></a>
            <!-- facebook icon -->
            <div class="fb-icon">
                <a href="https://www.facebook.com/Sixty6onPeterborough/"> <svg version="1.1"
                     xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                     x="0px" y="0px" width="56.7px" height="56.7px" viewBox="0 0 56.7 56.7" style="enable-background:new 0 0 56.7 56.7;"
                     xml:space="preserve">
                <style type="text/css">
                    .stfb {fill: #3289D9;}
                </style>
                                    <defs>
                                    </defs>
                                    <path class="stfb" d="M0,0v56.7h56.7V0H0z M37.8,16.5h-4.5c-1.5,0-2.5,0.6-2.5,2.1v2.6h7.1l-0.6,7.1h-6.5v18.9h-7.1V28.3h-4.7v-7.1
                    h4.7v-4.5c0-4.8,2.5-7.3,8.2-7.3h6V16.5z"/>
                </svg>
                     </a>
            </div>
            
        </div>
        <div class="col-xs-12 white-bar text-left header-text">
            <a class="brand" href="/"><img src="<?= get_template_directory_uri(); ?>/dist/images/sixty6-logo.png" alt=""></a>
            <span class="tagline"> <?= get_field('tag_line', 'option'); ?></span>
        <div class="vert-bar"></div>
        <button type="button" class="navbar-toggle" data-toggle="offcanvas" data-target=".navmenu">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        
        </div>

    </div>
</header>

<!--
<div class="navbar navbar-default navbar-inverse navbar-fixed-top hidden-xs">
    <a class="navbar-brand" href="/"><?= get_field( 'field_name', 'option' ); ?></a>

</div>
<div class="navbar navbar-fixed-top visible-xs">
    <button type="button" class="navbar-toggle pull-left " data-toggle="offcanvas" data-target=".navmenu">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
</div>
-->