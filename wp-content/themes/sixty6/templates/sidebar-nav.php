<?php
/**
 * Created by PhpStorm.
 * User: carl
 * Date: 26/02/17
 * Time: 11:38
 */

?>

<div class="navmenu navmenu-default navmenu-fixed-left offcanvas-sm nav-primary shadow-left" role="navigation">
    <div class="sidebar-nav">
        <a class="brand" href="/"><img src="<?= get_template_directory_uri(); ?>/dist/images/sixty6-logo.png" alt=""></a>
        
    
        <?php
        if ( has_nav_menu( 'primary_navigation' ) ) :
            wp_nav_menu( [ 'theme_location' => 'primary_navigation', 'menu_class' => 'nav navmenu-nav' ] );
        endif;
        ?>
	
	
	    <?php
	    if ( has_nav_menu( 'sidebar_foot_navigation' ) ) :
		    wp_nav_menu( [ 'theme_location' => 'sidebar_foot_navigation', 'menu_class' => 'nav sidebar-foot navmenu-nav' ] );
	    endif;
	    ?>
        <div class="socialicons">
            <ul>
                <li class="facebook"><a href="https://www.facebook.com/Sixty6onPeterborough/"><span class="icon-facebook_icon"></span></a></li>
                
                <li class="hidden-sm-down instagram"><a href="https://www.instagram.com/"><span class="icon-g"></span></a></li>
                <li class="twitter"><a href="https://twitter.com/"><span class="icon-twitter_icon"></span></a></li>
                <li class="hidden-sm-down youtube"><a href="https://www.youtube.com/"><span class="icon-youtube_icon"></span></a></li>

            </ul>
        </div>
     
    </div>
</div>