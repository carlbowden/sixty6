<?php use Roots\Sage\Titles; ?>

<div class="page-header">
    <h1><?= Titles\title(); ?></h1>
    <?php if(!is_archive()):?><h5><?= get_field('sub_heading');?></h5><?php endif; ?>
</div>
