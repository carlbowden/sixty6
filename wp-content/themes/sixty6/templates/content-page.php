<div class="col-sm-7 bg-page-grey ">
	<main class="content main <?php if ( get_field( 'show_under_content_panel' ) ): ?>with-under-content<?php endif; ?>">

		<?php get_template_part( 'templates/page', 'header' ); ?>
		<?php the_content(); ?>
        
	</main><!-- /.main -->
    
    <?php if(get_field('show_under_content_panel')): ?>
    
    <div class="under_content">
        <main class="content with-under-content">
            <h4><?= get_field( 'under_heading' ); ?></h4>
            <?= get_field( 'under_content' ); ?>
        </main>
    </div>
    
    <?php endif; ?>
    
</div>
<div class="col-sm-5 right-sidebar">
    <div class="top-gallery">
        
        <?php
        // loop through the banners
        $gallery = get_field('top_gallery');
        if( is_array( $gallery ) ) foreach ( $gallery as $image ): ?>
            <div class="gallery-slide" style="background-image: url(<?= $image['url']; ?>);">
            
            </div>
        <?php endforeach; ?>
        
    </div>
    <?php
    
    // check if we have video data
    
    
    $videos = get_field('right_video_copy');
    $vid    = $videos[0]['video_id'];
    if(empty($vid)){
        $videos = get_field('right_video'); //god knows
        $vid    = $videos[0]['video_id'];
    }
    
    ?>
    
    <div class="embed-container">
		<?php /*the_field( 'video' );*/ ?>
       
            <iframe style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; border: none; z-index: 100"
                    src="https://www.youtube.com/embed/<?= $vid ?>?controls=0&showinfo=0&rel=0&autoplay=0&loop=1" width="420" height="315"
                    allowfullscreen="allowfullscreen"></iframe>
    </div>
</div>

