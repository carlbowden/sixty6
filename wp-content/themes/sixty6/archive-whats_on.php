



<?php the_posts_navigation(); ?>
<div class="col-sm-7 bg-page-grey">
    <main class="content main">
        
        <?php get_template_part( 'templates/page', 'header' ); ?>
        <?php get_template_part( 'templates/content', 'events' ); ?>
        
        
    </main><!-- /.main -->
    

</div>
<div class="col-sm-5 right-sidebar">
    <div class="top-gallery">
        
        <?php
        // loop through the banners
        $gallery = get_field( 'top_gallery' );
        if(is_array($gallery)):
            foreach ( $gallery as $image ): ?>
                <div class="gallery-slide" style="  background-image: url(<?= $image['url']; ?>);"></div>
            <?php endforeach; ?>
        <?php else: ?>
            <div class="gallery-slide" style="background-image: url(/wp-content/uploads/2017/01/front-sixty6.jpg);"></div>
            <div class="gallery-slide" style="background-image: url(/wp-content/uploads/2017/01/img_1149.jpg);"></div>
            <div class="gallery-slide" style="background-image: url(/wp-content/uploads/2017/01/mashina-main.jpg);"></div>
            <div class="gallery-slide" style="background-image: url(/wp-content/uploads/2017/01/mashina140416_056.jpg);"></div>
            <div class="gallery-slide" style="background-image: url(/wp-content/uploads/2017/01/mashina140416_066.jpg);"></div>
        <?php endif; ?>
        
    </div>
    <div class="embed-container">
        <?php $video =  get_field( 'video' ); ?>
        <?php if(isset($video ) && $video):
            echo $video;
        else:?>
            <div id="fb-root"></div>
            <script>(function (d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) return;
                    js = d.createElement(s);
                    js.id = id;
                    js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.3";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>
            <div class="fb-video" data-href="https://www.facebook.com/Sixty6onPeterborough/videos/678533892312748/" data-width="640">
                <blockquote cite="https://www.facebook.com/Sixty6onPeterborough/videos/678533892312748/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Sixty6onPeterborough/videos/678533892312748/">Where Events Come to Life!</a>
                    <p></p>Posted by <a href="https://www.facebook.com/Sixty6onPeterborough/">Sixty6 on Peterborough</a> on Tuesday, 20 September 2016
                </blockquote>
            </div>
        <?php endif; ?>
    </div>
</div>
