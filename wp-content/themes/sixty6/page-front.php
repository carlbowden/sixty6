<?php
/**
 * Template Name: Home Page template
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', 'page-front'); ?>
<?php endwhile; ?>
