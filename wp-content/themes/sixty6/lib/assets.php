<?php

namespace Roots\Sage\Assets;

/**
 * Get paths for assets
 */
class JsonManifest {
  private $manifest;

  public function __construct($manifest_path) {
    if (file_exists($manifest_path)) {
      $this->manifest = json_decode(file_get_contents($manifest_path), true);
    } else {
      $this->manifest = [];
    }
  }

  public function get() {
    return $this->manifest;
  }

  public function getPath($key = '', $default = null) {
    $collection = $this->manifest;
    if (is_null($key)) {
      return $collection;
    }
    if (isset($collection[$key])) {
      return $collection[$key];
    }
    foreach (explode('.', $key) as $segment) {
      if (!isset($collection[$segment])) {
        return $default;
      } else {
        $collection = $collection[$segment];
      }
    }
    return $collection;
  }
}

function asset_path($filename) {
  $dist_path = get_template_directory_uri() . '/dist/';
  $directory = dirname($filename) . '/';
  $file = basename($filename);
  static $manifest;

  if (empty($manifest)) {
    $manifest_path = get_template_directory() . '/dist/' . 'assets.json';
    $manifest = new JsonManifest($manifest_path);
  }

  if (array_key_exists($file, $manifest->get())) {
    return $dist_path . $directory . $manifest->get()[$file];
  } else {
    return $dist_path . $directory . $file;
  }
}



// Register Custom Post Type
function whats_on_post_type() {
    
    $labels = array(
        'name'                  => _x( 'Whats On Items', 'Post Type General Name', 'text_domain' ),
        'singular_name'         => _x( 'Whats On Item', 'Post Type Singular Name', 'text_domain' ),
        'menu_name'             => __( 'Whats On', 'text_domain' ),
        'name_admin_bar'        => __( 'Whats On', 'text_domain' ),
        'archives'              => __( 'Whats On Archives', 'text_domain' ),
        'attributes'            => __( 'Whats On Attributes', 'text_domain' ),
        'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
        'all_items'             => __( 'All Whats On Items', 'text_domain' ),
        'add_new_item'          => __( 'Add New Whats On Item', 'text_domain' ),
        'add_new'               => __( 'Add Whats On', 'text_domain' ),
        'new_item'              => __( 'New Whats On Item', 'text_domain' ),
        'edit_item'             => __( 'Edit Whats On Item', 'text_domain' ),
        'update_item'           => __( 'Update Whats On Item', 'text_domain' ),
        'view_item'             => __( 'View Whats On Item', 'text_domain' ),
        'view_items'            => __( 'View Whats On Items', 'text_domain' ),
        'search_items'          => __( 'Search Whats On Item', 'text_domain' ),
        'not_found'             => __( 'No Whats On Items found', 'text_domain' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
        'featured_image'        => __( 'Whats On Featured Image', 'text_domain' ),
        'set_featured_image'    => __( 'Set Whats On featured image', 'text_domain' ),
        'remove_featured_image' => __( 'Remove Whats On featured image', 'text_domain' ),
        'use_featured_image'    => __( 'Use as Whats On featured image', 'text_domain' ),
        'insert_into_item'      => __( 'Insert into Whats On', 'text_domain' ),
        'uploaded_to_this_item' => __( 'Uploaded to this Whats On', 'text_domain' ),
        'items_list'            => __( 'Whats On Items list', 'text_domain' ),
        'items_list_navigation' => __( 'Whats On Items list navigation', 'text_domain' ),
        'filter_items_list'     => __( 'Filter Whats On items list', 'text_domain' ),
    );
    $args   = array(
        'label'               => __( 'Whats On Item', 'text_domain' ),
        'description'         => __( 'Items for the Whats On area', 'text_domain' ),
        'labels'              => $labels,
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'revisions', ),
        'taxonomies'          => array( 'category', 'post_tag' ),
        'hierarchical'        => FALSE,
        'public'              => TRUE,
        'show_ui'             => TRUE,
        'show_in_menu'        => TRUE,
        'menu_position'       => 5,
        'show_in_admin_bar'   => TRUE,
        'show_in_nav_menus'   => TRUE,
        'can_export'          => TRUE,
        'has_archive'         => 'whats-on-events',
        'exclude_from_search' => FALSE,
        'publicly_queryable'  => TRUE,
        'capability_type'     => 'page',
    );
    register_post_type( 'whats_on', $args );
    
}

add_action( 'init', __NAMESPACE__ . '\\whats_on_post_type', 0 );
add_filter( 'get_the_archive_title', function ( $title ) {
    
    if ( is_category() ) {
        
        $title = single_cat_title( '', FALSE );
        
    } elseif ( is_tag() ) {
        
        $title = single_tag_title( '', FALSE );
        
    } elseif ( is_author() ) {
        
        $title = '<span class="vcard">' . get_the_author() . '</span>';
        
    }
    
    return $title;
    
} );