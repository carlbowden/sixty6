<?php

namespace Roots\Sage\Setup;

use Roots\Sage\Assets;

/**
 * Theme setup
 */
function setup() {
  // Enable features from Soil when plugin is activated
  // https://roots.io/plugins/soil/
  add_theme_support('soil-clean-up');
  add_theme_support('soil-nav-walker');
  add_theme_support('soil-nice-search');
  add_theme_support('soil-jquery-cdn');
  add_theme_support('soil-relative-urls');

  // Make theme available for translation
  // Community translations can be found at https://github.com/roots/sage-translations
  load_theme_textdomain('sage', get_template_directory() . '/lang');

  // Enable plugins to manage the document title
  // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
  add_theme_support('title-tag');

  // Register wp_nav_menu() menus
  // http://codex.wordpress.org/Function_Reference/register_nav_menus
  register_nav_menus([
    'primary_navigation' => __('Primary Navigation', 'sage'),
    'sidebar_foot_navigation' => __( 'Sidebar Navigation Foot', 'sage' ),
	'footer_navigation' => __( 'Footer Navigation (black bar)', 'sage' ),
  ]);

  // Enable post thumbnails
  // http://codex.wordpress.org/Post_Thumbnails
  // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
  // http://codex.wordpress.org/Function_Reference/add_image_size
  add_theme_support('post-thumbnails');

  // Enable post formats
  // http://codex.wordpress.org/Post_Formats
  add_theme_support('post-formats', ['aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio']);

  // Enable HTML5 markup support
  // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
  add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

  // Use main stylesheet for visual editor
  // To add custom styles edit /assets/styles/layouts/_tinymce.scss
  add_editor_style(Assets\asset_path('styles/main.css'));
}
add_action('after_setup_theme', __NAMESPACE__ . '\\setup');

/**
 * Register sidebars
 */
function widgets_init() {
  register_sidebar([
    'name'          => __('Primary', 'sage'),
    'id'            => 'sidebar-primary',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ]);

  register_sidebar([
    'name'          => __('Footer', 'sage'),
    'id'            => 'sidebar-footer',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ]);
}
add_action('widgets_init', __NAMESPACE__ . '\\widgets_init');

/**
 * Determine which pages should NOT display the sidebar
 */
function display_sidebar() {
  static $display;

  isset($display) || $display = !in_array(true, [
    // The sidebar will NOT be displayed if ANY of the following return true.
    // @link https://codex.wordpress.org/Conditional_Tags
    is_404(),
    is_front_page(),
    is_page_template('template-custom.php'),
	  true //disable fro now
  ]);

  return apply_filters('sage/display_sidebar', $display);
}

/**
 * Theme assets
 */
function assets() {
	wp_enqueue_style('sage/css', Assets\asset_path('styles/main.css'), false, null);
	
	if (is_single() && comments_open() && get_option('thread_comments')) {
	wp_enqueue_script('comment-reply');
	}
	wp_enqueue_style( 'slick_css', 'https://cdn.jsdelivr.net/g/jquery.slick@1.6.0(slick-theme.css+slick.css)' );
	//wp_enqueue_script('google_maps_js', 'http://maps.google.com/maps/api/js', array('jquery'), '', TRUE);
	wp_enqueue_script( 'slick_js', 'https://cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js', array( 'jquery' ), '', TRUE );
	
	wp_enqueue_script( 'jasny/js', Assets\asset_path( '../bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js' ), [ 'jquery' ], NULL, TRUE );
	wp_enqueue_script('sage/js', Assets\asset_path('scripts/main.js'), ['jquery'], null, true);
  
}
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100);

if ( function_exists( 'acf_add_options_page' ) ) {
	
	acf_add_options_page();
	
}
function sixty6_acf_init() {
    
    acf_update_setting( 'google_api_key', 'AIzaSyC1BbPhYYSVcVPzsnwpA1_baD2RokZ7yd8' );
}

add_action( 'acf/init', __NAMESPACE__ . '\\sixty6_acf_init' );


remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );
remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20 );

remove_action( 'woocommerce_product_thumbnails', 'woocommerce_show_product_thumbnails', 20 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50 );
//remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);

remove_action( 'woocommerce_simple_add_to_cart', 'woocommerce_simple_add_to_cart', 30 );
remove_action( 'woocommerce_grouped_add_to_cart', 'woocommerce_grouped_add_to_cart', 30 );
//remove_action('woocommerce_variable_add_to_cart', 'woocommerce_variable_add_to_cart', 30);
remove_action( 'woocommerce_external_add_to_cart', 'woocommerce_external_add_to_cart', 30 );
//remove_action('woocommerce_single_variation', 'woocommerce_single_variation', 10);
//remove_action('woocommerce_single_variation', 'woocommerce_single_variation_add_to_cart_button', 20);

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

remove_action( 'woocommerce_review_before', 'woocommerce_review_display_gravatar', 10 );
remove_action( 'woocommerce_review_before_comment_meta', 'woocommerce_review_display_rating', 10 );
remove_action( 'woocommerce_review_meta', 'woocommerce_review_display_meta', 10 );
remove_action( 'woocommerce_review_comment_text', 'woocommerce_review_display_comment_text', 10 );

add_filter( 'woocommerce_get_availability', __NAMESPACE__ . '\\sixty6_custom_get_availability', 1, 2 );
function sixty6_custom_get_availability( $availability, $_product ) {
    //echo "<pre>" . print_r(get_class($_product), 1) . "</pre>";
    //echo "<pre>" . print_r($_product->list_attributes(), 1) . "</pre>";
    // Change In Stock Text
    
    $stock = preg_replace( '/^([0-9]+).*$/', "$1", $availability['availability'] );
    
    if ( $_product->is_in_stock() ) {
        $availability['availability'] = $stock . __( ' Tickets available', 'woocommerce' );
    }
    // Change Out of Stock Text
    if ( ! $_product->is_in_stock() ) {
        $availability['availability'] = __( 'Sorry, Sold Out', 'woocommerce' );
    }
    
    //echo "<pre>" . print_r($availability, 1) . "</pre>";
    return $availability;
}

/**
 * Clean up wp_nav_menu_args
 *
 * Remove the container
 * Remove the id="" on nav menu items
 */
function sixty6_nav_menu_args( $args = '' ) {
    $nav_menu_args              = [];
    $nav_menu_args['container'] = FALSE;
    
    if ( ! $args['items_wrap'] ) {
        $nav_menu_args['items_wrap'] = '<ul class="dropdown %2$s">%3$s</ul>';
    }
    
    if ( ! $args['walker'] ) {
        $nav_menu_args['walker'] = new  \Roots\Soil\Nav\NavWalker();
    }
    
    return array_merge( $args, $nav_menu_args );
}

add_filter( 'wp_nav_menu_args', __NAMESPACE__ . '\\sixty6_nav_menu_args' );

