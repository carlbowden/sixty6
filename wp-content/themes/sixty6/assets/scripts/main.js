/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages

          //
          $('.top-gallery').slick({
              dots: false,
              infinite: true,
              slidesToShow: 1,
              adaptiveHeight: true,
              autoplay: true,
              autoplaySpeed: 5000,
              speed: 1500,
              fade: true,
              cssEase: 'linear',
              //prevArrow: $('.banner-tools .prev'),
              //nextArrow: $('.banner-tools .next'),
              responsive: [
                  {
                      breakpoint: 994,
                      settings: {}
                  },
                  {
                      breakpoint: 600,
                      settings: {}
                  }
              ]
          });




          function onPlayerStateChange(e) {
              if (e.data === 1) {
                  $('#tv').addClass('active');
                  $('.hi em:nth-of-type(2)').html(currVid + 1);
              } else if (e.data === 2) {
                  $('#tv').removeClass('active');
                  if (currVid === vid.length - 1) {
                      currVid = 0;
                  } else {
                      currVid++;
                  }
                  tv.loadVideoById(vid[currVid]);
                  tv.seekTo(vid[currVid].startSeconds);
              }
          }

          function vidRescale() {

              // console.log(tv);
              //
              // // var w = $(window).width() + 200,
              // //     h = $(window).height() + 200;
              //
              // var w = $('.embed-container').width(),
              //     h = $('.embed-container').height();
              // console.log(h,w);


              // tv.setSize(w, h);
              // $('.tv .screen').css({'left': -($('.tv .screen').outerWidth() - w) / 2});

              // if (w / h > 16 / 9) {
              //     tv.setSize(w, w / 16 * 9);
              //     $('.tv .screen').css({'left': '0px'});
              // } else {
              //     tv.setSize(h / 9 * 16, h);
              //     $('.tv .screen').css({'left': -($('.tv .screen').outerWidth() - w) / 2});
              // }
          }


          $(window).on('load resize', function () {
              vidRescale();
          });

      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
          $('.up-comming-gallery').slick({
              dots: false,
              infinite: true,
              slidesToShow: 1,
              adaptiveHeight: true,
              autoplay: true,
              autoplaySpeed: 5000,
              speed: 1500,
              fade: true,
              cssEase: 'linear',
              responsive: [
                  {
                      breakpoint: 994,
                      settings: {}
                  },
                  {
                      breakpoint: 600,
                      settings: {}
                  }
              ]
          });

          $('.right_gallery').slick({
              dots: false,
              infinite: true,
              slidesToShow: 1,
              adaptiveHeight: true,
              autoplay: true,
              autoplaySpeed: 5000,
              speed: 1500,
              fade: true,
              cssEase: 'linear',
              responsive: [
                  {
                      breakpoint: 994,
                      settings: {}
                  },
                  {
                      breakpoint: 600,
                      settings: {}
                  }
              ]
          });
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
