<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) exit;

spl_autoload_register( 'WC_Gateway_Payment_Express_PxPay::autoload' );

class DpsPxPayException extends Exception {}
class DpsPxPayCurlException extends Exception {}

/**
 * @class 		WC_Gateway_Payment_Express_PxPay
 * @extends		WC_Payment_Gateway
 * @version		1.2
 */
class WC_Gateway_Payment_Express_PxPay extends WC_Payment_Gateway {

	public $pxpay;
	protected $paymentURL = false;		/* where to redirect browser for payment */
	protected $errorMessage = false;	/* last transaction error message */
	
	public function __construct() {
		
		global $woocommerce;

		$this->id   = 'payment_express';
		$this->method_title = __('Payment Express', 'woothemes');
		$this->icon   = '';
		$this->has_fields  = false;

		/* Load the form fields. */
		$this->init_form_fields();

		/* Load the settings. */
		$this->init_settings();

		/* Define user set variables */
		$this->title       		= $this->get_option( 'title' );	
		$this->description  	= $this->get_option( 'description' );
		$this->site_name       	= $this->get_option( 'site_name' );
		$this->access_userid 	= $this->get_option( 'access_userid' );
		$this->access_key       = $this->get_option( 'access_key' );
		$this->access_url       = $this->get_option( 'access_url' );
		
		$this->success_url       = $this->get_option( 'success_url' );
		$this->fail_url      	 = $this->get_option( 'fail_url' );

		add_filter( 'filter_pxpay_notification_orderID', array( $this, 'sequential_order_number_fixup' ) );

		/* Hook IPN callback logic*/
		new WC_Gateway_Payment_Express_PxPay_Notification_Handler( $this->access_userid, $this->access_key, $this->access_url );

		add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
		add_action( 'woocommerce_receipt_' . $this->id, array( $this, 'receipt_page' ) );

		add_action( 'woocommerce_email_before_order_table', array( $this, 'email_instructions' ), 10, 2);
		
		/* initiation of logging instance */
		$this->log = new WC_Logger();
	}

	/**
	 * Initialise Gateway Settings Form Fields
	 */
	function init_form_fields() {

		global $woocommerce;
		
		$default_site_name = home_url() ;

		$script_url = $woocommerce->api_request_url( get_class( $this ) );
		
		$this->form_fields = array(

			'enabled' => array(
				'title' => __( 'Enable/Disable', 'woothemes' ),
				'type' => 'checkbox',
				'label' => __( 'Enable Payment Express', 'woothemes' ),
				'default' => 'yes'
			),

			'title' => array(
				'title' => __( 'Title', 'woothemes' ),
				'type' => 'text',
				'description' => __( 'This controls the title which the user sees during checkout.', 'woothemes' ),
				'default' => __( 'Payment Express', 'woothemes' ),
				'css' => 'width: 400px;'
			),

			'description' => array(
				'title' => __( 'Description', 'woothemes' ),
				'type' => 'textarea',
				'description' => __( 'This controls the description which the user sees during checkout.', 'woothemes' ),
				'default' => __("Allows credit card payments by Payment Express PX-Pay method", 'woothemes')
			),

			'site_name' => array(
				//'title' => __( 'Px-Pay Access Key', 'woothemes' ),
				'title' => 'Merchant Reference',
				'description' => 'A name (or URL) to identify this site in the "Merchant Reference" field (shown when viewing transactions in the site\'s Digital Payment Express back-end). This name <b>plus</b> the longest Order/Invoice Number used by the site must be <b>no longer than 53 characters</b>.',
				'type' => 'text',
				'default' => $default_site_name,
				'css' => 'width: 400px;',
				'custom_attributes' => array( 'maxlength' => '53' )
			),

			'access_userid' => array(
				//'title' => __( 'Access User Id', 'woothemes' ),
				'title' => __( 'Px-Pay Access User ID', 'woothemes' ),
				'type' => 'text',
				'default' => '',
				'css' => 'width: 400px;'
			),

			'access_key' => array(
				//'title' => __( 'Access Key', 'woothemes' ),
				'title' => __( 'Px-Pay Access Key', 'woothemes' ),
				'type' => 'text',
				'default' => '',
				'css' => 'width: 400px;'
			),
			
			'access_url' => array(
				'title' => __( 'Px-Pay URL', 'woothemes' ),
				'description' => __( 'To use the older PXPay payment page please use https://sec.paymentexpress.com/pxpay/pxaccess.aspx in the text field above.', 'woothemes'),
				'type' => 'text',
				'default' => 'https://sec.paymentexpress.com/pxaccess/pxpay.aspx',
				'css' => 'width: 400px;'
			),
			
			'success_url' => array(
				'title' => __( 'Success URL', 'woothemes' ),
				'description' => __( 'User will be returned to this page after successful transaction on DPS payment page.', 'woothemes'),
				'type' => 'text',
				'default' => $script_url,
				'css' => 'width: 400px;'
			),
			
			'fail_url' => array(
				'title' => __( 'Failed URL', 'woothemes' ),
				'description' => __( 'User will be returned to this page after failed transaction on DPS payment page.', 'woothemes'),
				'type' => 'text',
				'default' => $script_url,
				'css' => 'width: 400px;'
			),
		);

	} /* End init_form_fields() */

	/**
	 * Admin Panel Options
	 * - Options for bits like 'title' and availability on a country-by-country basis
	 *
	 * @since 1.0.0
	 */
	public function admin_options() {
		?>
		<h3><?php _e('Payment Express', 'woothemes'); ?></h3>
		<p><?php _e('Allows credit card payments by Payment Express PX-Pay method', 'woothemes'); ?></p>
		<table class="form-table">
		<?php
			// Generate the HTML For the settings form.
			$this->generate_settings_html();
		?>
		</table><!--/.form-table-->
		<?php
	} /* End admin_options() */

	
	/**
	 * There are no payment fields for paypal, but we want to show the description if set.
	 **/
	function payment_fields() {
		if ($this->description) echo wpautop(wptexturize($this->description));
	}

	/**
	 * receipt_page
	 **/
	function receipt_page( $order_id ) {
		/*echo '<p>'.__('Thank you for your order, you are now being redirected to Payment Express.', 'woothemes').'</p>';*/

		global $woocommerce;
		$order         = wc_get_order( $order_id );
		$order_number  = $order->get_order_number();
		$billing_name  = $order->billing_first_name." ".$order->billing_last_name;
		$shipping_name = explode(' ', $order->shipping_method);

		$http_host   = getenv("HTTP_HOST");
		$request_uri = getenv("SCRIPT_NAME");
		$server_url  = "http://$http_host";

		$urlFail = $woocommerce->cart->get_checkout_url();
		
		if( function_exists ( "get_woocommerce_currency" ) ){
			$currency = get_woocommerce_currency(); 
		} else {
			$currency = get_option('woocommerce_currency');
		}
		$currency = apply_filters( 'filter_pxpay_checkout_currency', $currency, $order_id, $this->settings );
		
		//$MerchantRef = home_url();
		//$MerchantRef.= " # ".$order->order_key;
		$MerchantRef = $this->site_name . ' - Order # ' . $order_number ;
		if ( strlen( $MerchantRef ) > 64 ) {
			$MerchantRef = substr( $this->site_name , 0 , max( 50 - strlen( $order_number ) , 0 ) ) . '... - Order # ' . $order_number ;
			if ( strlen( $MerchantRef ) > 64 ) {
				$MerchantRef = 'Order # ' . substr( $order_numberd , 0 , 53 ) . '...' ;
			}
		}

		//Generate a unique identifier for the transaction
		$TxnId = uniqid("ID") ;
		$TxnId = $TxnId .'-'. $order_id;
		
		$TxnId =  apply_filters( 'filter_txn_id', $TxnId, $order );
		$MerchantRef =  apply_filters( 'filter_merchant_reference', $MerchantRef, $order );
		$txndata1 =  apply_filters( 'filter_custom_order_number', "Order number : ". $order_number );
				
		$success_url = apply_filters( 'filter_custom_success_url', $this->success_url, $order );
		$fail_url = apply_filters( 'filter_custom_fail_url', $this->fail_url, $order );
		
		$paymentReq = new DpsPxPayPayment( $this->access_userid, $this->access_key, $this->access_url );
		$paymentReq->txnType			= 'Purchase';
		$paymentReq->amount				= $order->order_total;
		$paymentReq->currency			= $currency;
		$paymentReq->transactionNumber	= $TxnId;
		$paymentReq->invoiceReference	= $MerchantRef;
		$paymentReq->option1			= $txndata1;
		$paymentReq->option2			= $billing_name;
		$paymentReq->option3			= $order->billing_email;
		$paymentReq->invoiceDescription	= $MerchantRef;
		$paymentReq->emailAddress		= $order->billing_email;
		$paymentReq->urlSuccess			= $success_url;
		$paymentReq->urlFail			= $fail_url;

		// allow plugins/themes to modify invoice description and reference, and set option fields
		$paymentReq->invoiceDescription	= apply_filters('dpspxpay_invoice_desc', $paymentReq->invoiceDescription, $order_id);
		$paymentReq->invoiceReference	= apply_filters('dpspxpay_invoice_ref', $paymentReq->invoiceReference, $order_id);
		$paymentReq->option1			= apply_filters('dpspxpay_invoice_txndata1', $paymentReq->option1, $order_id);
		$paymentReq->option2			= apply_filters('dpspxpay_invoice_txndata2', $paymentReq->option2, $order_id);
		$paymentReq->option3			= apply_filters('dpspxpay_invoice_txndata3', $paymentReq->option3, $order_id);
		
		$this->log->add( 'pxpay', '========= initiating transaction request' );
		$this->log->add( 'pxpay', sprintf( '%s account, invoice ref: %s, transaction: %s, amount: %s', 
			'test or live',
			$paymentReq->invoiceReference, $paymentReq->transactionNumber, $paymentReq->amount ) );

		$this->log->add( 'pxpay', sprintf( 'success URL: %s', $paymentReq->urlSuccess ) );
		$this->log->add( 'pxpay', sprintf( 'failure URL: %s', $paymentReq->urlFail ) );
		
		$this->errorMessage = '';
		try {
			$response = $paymentReq->processPayment();

			if ($response->isValid) {
				$this->paymentURL = $response->paymentURL;
			}
			else {
				$this->errorMessage = 'Payment Express request invalid.';
				$this->log->add( 'pxpay', $this->errorMessage );
			}
		}
		catch (DpsPxPayException $e) {
			$this->errorMessage = $e->getMessage();
			$this->log->add( 'pxpay', $this->errorMessage );
		}
		$this->log->add( 'pxpay', $this->paymentURL );
		$dps_adr =  $this->paymentURL;
				
		$img_loader = apply_filters( 'filter_custom_loader_image', plugins_url( 'images/ajax-loader.gif', __FILE__ ) );

$this->log->add( 'pxpay', print_r( array( 'dps url' => esc_url( $dps_adr ), 'ajax img_loader' => $img_loader ), true ) );
 
		echo '<form action="'.esc_url( $dps_adr ).'" method="post" id="dps_payment_form">
			<input type="submit" class="button-alt button alt" id="submit_Payment_Express_payment_form" value="'.__('Pay via Payment_Express', 'woothemes').'" /> <a class="button cancel" href="'.esc_url( $order->get_cancel_order_url() ).'">'.__('Cancel order &amp; restore cart', 'woothemes').'</a>
			<script type="text/javascript">
				jQuery(function(){
					jQuery("body").block(
						{
							message: "<img src=\"'. $img_loader .'\" alt=\"Redirecting...\" style=\"float:left; margin-right: 10px;\" />'.__('Thank you for your order. We are now redirecting you to Payment Express to make payment.', 'woothemes').'",
							overlayCSS:
							{
								background: "#fff",
								opacity: 0.6
							},
							css: {
								padding:        20,
								textAlign:      "center",
								color:          "#555",
								border:         "3px solid #aaa",
								backgroundColor:"#fff",
								cursor:         "wait",
								lineHeight:		"32px"
							}
						});
					jQuery("#submit_Payment_Express_payment_form").click();
				});
			</script>
		</form>';

	}

	/**
	 * Process the payment and return the result
	 **/
	function process_payment( $order_id ) {
		$order = wc_get_order( $order_id );

		return array(
			'result' => 'success',
			'redirect' => $order->get_checkout_payment_url( true )
		); 

	}

	function email_instructions( $order, $sent_to_admin ) {
		if ( $sent_to_admin ) return;

		if ( $order->status !== 'on-hold') return;

		if ( $order->payment_method !== 'Payment_Express') return;

		if ($this->description) echo wpautop(wptexturize($this->description));
	}


	/**
	* generalise an XML post request
	* @param string $url
	* @param string $request
	* @param bool $sslVerifyPeer whether to validate the SSL certificate
	* @return string
	* @throws DpsPxPayCurlException
	*/
	public static function xmlPostRequest($url, $request, $sslVerifyPeer = true) {
		// execute the request, and retrieve the response
		$response = wp_remote_post($url, array(
			'user-agent'	=> 'DPS PxPay 1.94',
			'sslverify'		=> $sslVerifyPeer,
			'timeout'		=> 60,
			'headers'		=> array(
									'Content-Type'		=> 'text/xml; charset=utf-8',
							   ),
			'body'			=> $request,
		));

		if (is_wp_error($response)) {
			throw new DpsPxPayCurlException($response->get_error_message());
		}

		return $response['body'];
	}
	
	/**
	* autoload classes as/when needed
	*
	* @param string $class_name name of class to attempt to load
	*/
	public static function autoload($class_name) {

		static $classMap = array (
			'DpsPxPayPayment'	=> 'includes/class.DpsPxPayPayment.php',
			'DpsPxPayResult'	=> 'includes/class.DpsPxPayResult.php',
			'WC_Gateway_Payment_Express_PxPay_Notification_Handler' => 'includes/class.DpsPxPayNotificationHandler.php', 
		);

		if (isset($classMap[$class_name])) {
			require DPSPXPAY_PLUGIN_ROOT . $classMap[$class_name];
		}
	}
	
	public function sequential_order_number_fixup( $orderNumber ){
		/*search for the order by custom order number*/
		$query_args = array(
			'numberposts' => 1,
			'meta_key'    => '_order_number_formatted',
			'meta_value'  => $orderNumber,
			'post_type'   => 'shop_order',
			'post_status' => 'any',
			'fields'      => 'ids',
		);

		$posts = get_posts( $query_args );
		
		list( $order_id ) = ! empty( $posts ) ? $posts : null;

$this->log->add( 'pxpay', print_r( array( 'posts' => $posts, 'order_id' => $order_id, 'orderNumber' => $orderNumber ), true ) );

		if ( null === $order_id ) { $order_id = $orderNumber; }
		
		return $order_id;
	}
	
}