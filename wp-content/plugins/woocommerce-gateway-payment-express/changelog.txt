*** Payment Express (PxPay) Changelog ***

2016.08.04 - version 2.2
* Added support for WooCommerce Sequential Order Numbers plugin.

2016.07.29 - version 2.1
* Two new fields added for success and failure URLs on setting apge.
* Improved the code for the efficiency.

2016.05.27 - version 2.0
* Fixed a bug which keeps site reloading when customer returns after a failed payment.
* Now plugin is compatible with Woocommerce 2.6.1
 
2016.05.21 - version 1.94
 * using get_woocommerce_currency() function to improve the compatibility with 3rd party plugins.

 2016.03.15 - version 1.93
 * Included Ajax loading image within plugin files as it was removed from Woo image directory.
 
2016.01.26 - version 1.92
 * using get_woocommerce_currency() function to improve the compatibility with 3rd party plugins.

2015.10.15 - Version 1.91
 * Added textfield to let admin change PxPay access url.

2015.05.29 - Version 1.9
 * Added support to use Order numbers instead of Post ID.

2014.09.03 - Version 1.8
 * Tidied up checkout process redirection.
 
2014.01.17 - Version 1.7
 * Added support for WooCommerce 2.1 & WordPress 3.8

2013.10.11 - Version 1.6.2
 * Added "htmlentities()" around the billing name sent to DPS.  As otherwise,
   any "&" in the name caused the redirect to DPS to loop infinitely.

2013.08.08 - Version 1.6
 * Re-applied changes that were lost when WooCommerce reverted the plugin back to Version 1.2
 * Fixed 64-character DPS "Merchant Reference" bug
 * Removed "Access URL" field from Admin Settings page
 * Added "Merchant Reference" field to Admin Settings page

2013.06.21 - version 1.5
 *Addressed issues with newer versions of Wordpress.

2013.04.10 - version 1.3
 * Now supports Woocommerce 2.0.

2013.02.12 - version 1.2
 * Invoice number now posted to Payment Express.
 * Small visual improvements.

2012.12.05 - version 1.1.1
 * Updater

2012.02.09 - version 1.0
 * First Release
