=== Tickera - Event Calendar Add-on ===
Contributors: tickera
Requires at least: 4.1
Tested up to: 4.3.1
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Event Calendar is used to display Tickera events in a nicely styled calendar.

== Description ==

Event Calendar is used to display Tickera events in a nicely styled calendar. Users can browse through your calendar of events and see when and for how long does the event lasts.
It's easy to add calendar to your page just by selecting it in Tickera shortcodes.
Predefined colors help you easily choose whichever style suits your website!

Help your users to check what events you have in future or brag about the events you had in past!

== Changelog ==

= 1.0.1.2 - 07/DEC/2015 =
- Added language selector for 46 languages (located in the shortcode builder)

= 1.0.1.1 - 23/NOV/2015 =
- Added automatic updater

= 1.0.1 - 21/OCT/2015 =
- Fixed issue with the shortcode position (output buffering)

= 1.0 - 13/OCT/2015 =
- First Release