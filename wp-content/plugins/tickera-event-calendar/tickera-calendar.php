<?php
/*
  Plugin Name: Event Calendar for Tickera
  Plugin URI: http://tickera.com/
  Description: Add calendar view for all your Tickera events
  Author: Tickera.com
  Author URI: http://tickera.com/
  Version: 1.0.1.2
  TextDomain: tc
  Domain Path: /languages/

  Copyright 2015 Tickera (http://tickera.com/)
 */

if ( !defined( 'ABSPATH' ) )
	exit; // Exit if accessed directly


if ( !class_exists( 'TC_Event_Calendar' ) ) {

	class TC_Event_Calendar {

		var $version		 = '1.0.1.2';
		var $title		 = 'Calendar';
		var $name		 = 'tc_calendar';
		var $dir_name	 = 'tickera-event-calendar';
		var $location	 = 'plugins';
		var $plugin_dir	 = '';
		var $plugin_url	 = '';

		function __construct() {
			$this->init_vars();
			$this->init();
		}

		function init_vars() {
//setup proper directories
			if ( defined( 'WP_PLUGIN_URL' ) && defined( 'WP_PLUGIN_DIR' ) && file_exists( WP_PLUGIN_DIR . '/' . $this->dir_name . '/' . basename( __FILE__ ) ) ) {
				$this->location		 = 'subfolder-plugins';
				$this->plugin_dir	 = WP_PLUGIN_DIR . '/' . $this->dir_name . '/';
				$this->plugin_url	 = plugins_url( '/', __FILE__ );
			} else if ( defined( 'WP_PLUGIN_URL' ) && defined( 'WP_PLUGIN_DIR' ) && file_exists( WP_PLUGIN_DIR . '/' . basename( __FILE__ ) ) ) {
				$this->location		 = 'plugins';
				$this->plugin_dir	 = WP_PLUGIN_DIR . '/';
				$this->plugin_url	 = plugins_url( '/', __FILE__ );
			} else if ( is_multisite() && defined( 'WPMU_PLUGIN_URL' ) && defined( 'WPMU_PLUGIN_DIR' ) && file_exists( WPMU_PLUGIN_DIR . '/' . basename( __FILE__ ) ) ) {
				$this->location		 = 'mu-plugins';
				$this->plugin_dir	 = WPMU_PLUGIN_DIR;
				$this->plugin_url	 = WPMU_PLUGIN_URL;
			} else {
				wp_die( sprintf( __( 'There was an issue determining where %s is installed. Please reinstall it.', 'tc' ), $this->title ) );
			}
		}

		function init() {
			global $tc;

			require_once($this->plugin_dir . 'includes/functions.php');

			//add_action( 'wp_enqueue_scripts', array( &$this, 'front_scripts_and_styles' ) );

			add_action( 'admin_enqueue_scripts', array( &$this, 'admin_scripts_and_styles' ) );
			//localize the plugin
			add_action( 'init', array( &$this, 'localization' ), 10 );

			add_shortcode( 'tc_calendar', array( &$this, 'show_calendar' ) );

			add_filter( 'tc_event_fields', array( &$this, 'tc_add_event_fields' ) );

			add_filter( 'tc_shortcodes', array( &$this, 'tc_shortcodes_to_shortcode_builder' ) );
		}

		function localization() {

// Load up the localization file if we're using WordPress in a different language
// Place it in this plugin's "languages" folder and name it "tc-[value in wp-config].mo"
			if ( $this->location == 'mu-plugins' ) {
				load_muplugin_textdomain( 'tc', 'languages/' );
			} else if ( $this->location == 'subfolder-plugins' ) {
				//load_plugin_textdomain( 'tc', false, $this->plugin_dir . '/languages/' );
				load_plugin_textdomain( 'tc', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
			} else if ( $this->location == 'plugins' ) {
				load_plugin_textdomain( 'tc', false, 'languages/' );
			} else {
				
			}

			$temp_locales	 = explode( '_', get_locale() );
			$this->language	 = ($temp_locales[ 0 ]) ? $temp_locales[ 0 ] : 'en';
		}

		function front_scripts_and_styles( $scheme, $lang ) {
			$color_schemes = tc_get_calendar_color_schemes();

			$selected_color_scheme = $color_schemes[ $scheme ];

			wp_enqueue_style( $this->name . '-fullcalendar', $this->plugin_url . 'includes/fullcalendar/fullcalendar.css', array(), $this->version );
			wp_enqueue_style( $this->name . '-front', $this->plugin_url . 'includes/css/front.css', array(), $this->version );

			if ( $selected_color_scheme[ 'url' ] !== '' ) {
				wp_enqueue_style( $this->name . '-' . $selected_color_scheme[ 'name' ], $selected_color_scheme[ 'url' ], array(), $this->version );
			}

			wp_enqueue_script( $this->name . '-moment', $this->plugin_url . 'includes/fullcalendar/lib/moment.min.js', array( 'jquery' ), $this->version, true );
			wp_enqueue_script( $this->name . '-fullcalendar', $this->plugin_url . 'includes/fullcalendar/fullcalendar.js', array( $this->name . '-moment', 'jquery' ), $this->version, true );
			wp_enqueue_script( $this->name . '-fullcalendar-lang', $this->plugin_url . 'includes/fullcalendar/lang/' . $lang . '.js', array( $this->name . '-fullcalendar' ), $this->version, true );
		}

		function admin_scripts_and_styles() {
			wp_enqueue_style( $this->name . '-admin', $this->plugin_url . 'css/admin.css', array(), $this->version );
			wp_enqueue_script( $this->name . '-admin', $this->plugin_url . 'js/admin.js', array( 'jquery' ), $this->version );
		}

		function show_calendar( $atts ) {
			ob_start();
			extract( shortcode_atts( array(
				'show_past_events'	 => '',
				'id'				 => '',
				'color_scheme'		 => 'default',
				'lang'				 => 'en'
			), $atts ) );

			$this->front_scripts_and_styles( $color_scheme, $lang );

			$calendar_id		 = isset( $calendar_id ) && !empty( $calendar_id ) ? 'tc_calendar_' . $calendar_id : 'tc_calendar';
			$wp_events_search	 = new TC_Events_Search( '', '', -1, 'publish' );
			?>
			<script>
				jQuery( document ).ready( function() {

					jQuery( '#<?php echo $calendar_id; ?>' ).fullCalendar( {
						header: {
							left: 'prev,next today',
							center: 'title',
							right: 'month,agendaWeek,agendaDay',
						},
						defaultDate: '<?php echo date( "Y-m-d" ); ?>',
						editable: false,
						eventLimit: false, // allow "more" link when too many events
						events: [
			<?php
			foreach ( $wp_events_search->get_results() as $event ) {
				$event = new TC_Event( $event->ID );

				$event_presentation_page = get_post_meta( $event->details->ID, 'event_presentation_page', true );

				if ( !empty( $event_presentation_page ) && is_numeric( $event_presentation_page ) ) {
					$event_url = html_entity_decode( addslashes( get_permalink( $event_presentation_page ) ) );
				} else {
					$event_url = '';
				}

				if ( !empty( $event->details->event_end_date_time ) ) {
					echo "{
title: '" . html_entity_decode( addslashes( $event->details->post_title ) ) . "',
start: '" . esc_attr( $event->details->event_date_time ) . "',
end: '" . esc_attr( $event->details->event_end_date_time ) . "',
url: '" . $event_url . "',
},";
				} else {
					echo "{
title: '" . html_entity_decode( addslashes( $event->details->post_title ) ) . "',
start: '" . esc_attr( $event->details->event_date_time ) . "',
url: '" . $event_url . "',
},";
				}
			}
			?>
						]
					} );

				} );
			</script>
			<div id='<?php echo esc_attr( $calendar_id ); ?>'></div>
			<?php
			$content = ob_get_clean();
			return $content;
		}

		function tc_add_event_fields( $fields ) {
			$fields[] = array(
				'field_name'		 => 'event_presentation_page',
				'field_title'		 => __( 'Event Presentation Post / Page', 'tc' ),
				'placeholder'		 => '',
				'field_type'		 => 'function',
				'function'			 => 'tc_get_posts_and_pages',
				'field_description'	 => __( 'Select an event presentation post or page. Selected page will be link to the event in the calendar.', 'tc' ),
				'table_visibility'	 => false,
				'post_field_type'	 => 'post_meta'
			);
			return $fields;
		}

		function tc_shortcodes_to_shortcode_builder( $shortcodes ) {
			$shortcodes[ 'tc_calendar' ] = __( 'Display event calendar', 'tc' );
			return $shortcodes;
		}

	}

}

$tc_event_calendar = new TC_Event_Calendar();

//Addon updater 

global $tc;
$tc_general_settings = get_option( 'tc_general_setting', false );

$addon_slug = 'tickera-event-calendar';

if ( !defined( 'TC_NU' ) ) {//updates are allowed
	$license_key = (defined( 'TC_LCK' ) && TC_LCK !== '') ? TC_LCK : (isset( $tc_general_settings[ 'license_key' ] ) && $tc_general_settings[ 'license_key' ] !== '' ? $tc_general_settings[ 'license_key' ] : '');

	if ( $license_key !== '' ) {
		$updater_file = $tc->plugin_dir . 'includes/plugin-update-checker/plugin-update-checker.php';
		if ( file_exists( $updater_file ) ) {
			require_once($updater_file);
			$tc_plugin_update_checker = PucFactory::buildUpdateChecker( 'https://tickera.com/update/?action=get_metadata&slug=' . $addon_slug, __FILE__, $addon_slug, 1 );
		}
	}
}
